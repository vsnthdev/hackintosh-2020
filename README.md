# Guide
Read 👉 https://dortania.github.io/OpenCore-Install-Guide/extras/big-sur/

# Firmware Drivers
1. [HFSPlus.efi](https://github.com/acidanthera/OcBinaryData/blob/master/Drivers/HfsPlus.efi?raw=true)
2. OpenRuntime.efi (bundled with OpenCore)
3. OpenCanopy.efi (bundled with OpenCore)

# Kexts

1. <a target="_blank" rel="noopener" href="https://github.com/acidanthera/AppleALC/releases">AppleALC</a>
2. <a target="_blank" rel="noopener" href="https://github.com/acidanthera/IntelMausi/releases">IntelMausi</a>
3. <a target="_blank" rel="noopener" href="https://github.com/acidanthera/Lilu/releases">Lilu</a>
4. <a target="_blank" rel="noopener" href="https://bitbucket.org/RehabMan/os-x-usb-inject-all/downloads/">USBInjectAll</a>
5. <a target="_blank" rel="noopener" href="https://github.com/acidanthera/VirtualSMC/releases">VirtualSMC</a>
6. <a target="_blank" rel="noopener" href="https://github.com/acidanthera/WhateverGreen/releases">WhateverGreen</a>

# Resources
Download <a target="_blank" rel="noopener" href="https://github.com/acidanthera/OcBinaryData/tree/master/Resources">Resources Directory</a> and install install in OpenCore.